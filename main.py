from googletrans import Translator
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfpage import PDFTextExtractionNotAllowed
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from io import StringIO
from pdfminer.layout import LAParams
from pdfminer.converter import TextConverter


class MyParser(object):
    def __init__(self, pdf):
        parser = PDFParser(open(pdf, 'rb'))
        document = PDFDocument(parser)
        if not document.is_extractable:
            raise PDFTextExtractionNotAllowed
        rsrcmgr = PDFResourceManager()
        retstr = StringIO()
        laparams = LAParams()
        codec = 'utf-8'
        device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        for page in PDFPage.create_pages(document):
            interpreter.process_page(page)
        self.records = []
        lines = retstr.getvalue().splitlines()
        for line in lines:
            self.handle_line(line)

    def handle_line(self, line):
        self.records.append(line)


def read_pdf():
    p = MyParser('MusicRoom.fr.pdf')
    return '\n'.join(p.records)


def googletrans(text):
    split_text = text.split('\n')
    for line in split_text:
        translator = Translator()
        translat_text = translator.translate(str(line), src='fr', dest='ru')
        print(translat_text.text)


def main():
    text = read_pdf()
    googletrans(text)


main()